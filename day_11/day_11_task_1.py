# Day 11 Task 1
# Strategy:
# Difficulty: Easy, only slightly tricky with coordinates when dealing with lists of lists
#   Should be easier with numpy arrays..


def calc_single_power_level(x, y, grid_serial_number):
    power_level = (x + 10) * y + grid_serial_number
    power_level = (x + 10) * power_level
    power_level = int(str(power_level).zfill(3)[-3]) - 5
    return power_level


def calc_grid_power_levels(grid_serial_number: int):
    grid = []
    # NOTE: xi, yi are list indices, to convert them to X, Y coordinates
    #   as required by the task, you need to +1.
    for yi in range(300):
        row = []
        for xi in range(300):
            row.append(calc_single_power_level(xi + 1, yi + 1, grid_serial_number))
        grid.append(row)
    return grid


def find_nxn_max(grid, n=3):
    max_power = -1
    for yi in range(300 - n):
        for xi in range(300 - n):
            subgrid_sum = sum(sum(row[xi:xi + n]) for row in grid[yi:yi + n])
            if subgrid_sum > max_power:
                max_power = subgrid_sum
                max_topleft_coords = (xi + 1, yi + 1)
    return max_power, max_topleft_coords


def main():
    # Unit Tests
    # assert calc_single_power_level(3, 5, 8) == 4
    # assert calc_single_power_level(122, 79, 57) == -5
    # assert calc_single_power_level(217, 196, 39) == 0
    # assert calc_single_power_level(101, 153, 71) == 4

    # Grid Test
    # grid = calc_grid_power_levels(18)
    # print(*[row[31:36] for row in grid[43:48]], sep='\n')
    # print("Top left cell:", grid[45 - 1][33 - 1])  # NOTE: grid[Y-1][X-1], where X,Y is the coordinates

    grid = calc_grid_power_levels(1308)
    power, coords = find_nxn_max(grid, n=3)
    print(f"3x3 grid with the largest power={power} has top-left coordinates {coords}")


if __name__ == '__main__':
    main()
