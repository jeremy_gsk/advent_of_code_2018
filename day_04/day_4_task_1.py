# Day 4 Task 1
# Assumptions:
# 	- datetime is always formatted like [...] with 18 characters within
import re
from datetime import datetime
from collections import Counter, defaultdict


def main():
    # pre-compile regex
    regex_id = re.compile(r'(?:Guard #)(\d+)')
    # initialize dictionary;
    d = defaultdict(Counter)

    with open('day_04/day_4_input.txt') as f:
        all_cmds = f.read().split('\n')
    # reorder according to datetime
    all_cmds = sorted(all_cmds,
                      key=lambda cmd: datetime.strptime(cmd[:18], '[%Y-%m-%d %H:%M]'))

    for cmd in all_cmds:
        cmd_dt = datetime.strptime(cmd[:18], '[%Y-%m-%d %H:%M]')
        is_guard = re.search(regex_id, cmd)

        if is_guard is not None:
            guard_id = is_guard.group(1)
        elif 'asleep' in cmd:
            sleep_start = cmd_dt.minute
        elif 'wakes' in cmd:
            sleep_end = cmd_dt.minute
            d[guard_id].update(range(sleep_start, sleep_end))

    sleepiest_guard = max(d, key=lambda k: sum(d[k].values()))
    most_slept_minute = d[sleepiest_guard].most_common(1)[0][0]

    print(f"Guard ID who slept the most: {sleepiest_guard}\n"
          f"Minute most slept: {most_slept_minute}")

    print(f"Answer is: {int(sleepiest_guard)*most_slept_minute}")

    # -- save ordered cmds to file for quick reference
    # with open('day_4_reordered_input.txt', mode='w') as f:
    #     f.write('\n'.join(list(all_cmds)))


if __name__ == '__main__':
    main()
