# Day 4 Task 2
import re
from datetime import datetime
from collections import Counter, defaultdict


def main():
    # pre-compile regex
    regex_id = re.compile(r'(?:Guard #)(\d+)')
    # initialize dictionary: {minute: Counter(guard_id: count)};
    d = defaultdict(Counter)

    with open('day_04/day_4_input.txt') as f:
        all_cmds = f.read().split('\n')
    # reorder according to datetime
    all_cmds = sorted(all_cmds,
                      key=lambda cmd: datetime.strptime(cmd[:18], '[%Y-%m-%d %H:%M]'))

    for cmd in all_cmds:
        cmd_dt = datetime.strptime(cmd[:18], '[%Y-%m-%d %H:%M]')
        is_guard = re.search(regex_id, cmd)

        if is_guard is not None:
            guard_id = is_guard.group(1)
        elif 'asleep' in cmd:
            sleep_start = cmd_dt.minute
        elif 'wakes' in cmd:
            sleep_end = cmd_dt.minute
            for m in range(sleep_start, sleep_end):
                d[m].update([guard_id])

    most_slept_minute = sorted(d.keys(), reverse=True,
                               key=lambda x: d[x].most_common(1)[0][1])[0]
    sleepiest_guard = d[most_slept_minute].most_common(1)[0][0]

    print(f"Answer: {int(sleepiest_guard)*most_slept_minute}")


if __name__ == '__main__':
    main()
