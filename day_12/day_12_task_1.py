# Day 12 Task 1
# Strategy:
# Difficulty:
sample_input = """
initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #
"""
NUM_GENERATIONS = 20


def main():
    with open('day_12/day_12_input.txt') as f:
        # str_input = sample_input.strip().split('\n')
        str_input = f.read().strip().split('\n')

    state = str_input[0].replace("initial state: ", "")
    plant_combinations = [tuple(combi.split(" => ")) for combi in str_input[2:]]

    for g in range(1, NUM_GENERATIONS + 1):
        state = '....' + state + '....'
        next_state = list(state)
        for idx in range(2, len(state) - 2):
            candidate_state = state[idx - 2:idx + 3]
            for combi, result in plant_combinations:
                if candidate_state == combi:
                    next_state[idx] = result
                    break
            else:
                # just for sample_input (because given combi is not complete)
                next_state[idx] = '.'

        # updated state as of generation g
        state = ''.join(next_state)
        # print(f"{g:02d}: {''.join(state)}")

    total_val = 0
    for val, char in enumerate(state, start=-4 * NUM_GENERATIONS):
        if char == '#':
            total_val += val

    print(total_val)


if __name__ == '__main__':
    main()
