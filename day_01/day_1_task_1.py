# Day 1 Task 1
OPS = {'+': lambda a, b: a + b, '-': lambda a, b: a - b}


def parser(cmd: str, c: int):
    parsed_cmd = OPS[cmd[0]](c, int(cmd[1:]))
    return parsed_cmd


def main():
    running_count = 0
    with open('day_01/day_1_input.txt') as f:
        all_cmds = f.read().split('\n')
    for cmd in all_cmds:
        # print(f"Running Count: {running_count}")
        # print(f"Data string: {cmd}")
        running_count = parser(cmd, running_count)
    print(f"Final result: {running_count}")


if __name__ == '__main__':
    main()
