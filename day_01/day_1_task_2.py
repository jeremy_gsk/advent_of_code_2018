# Day 1 Task 2
OPS = {'+': lambda a, b: a + b, '-': lambda a, b: a - b}


def parser(cmd: str, c: int):
    parsed_cmd = OPS[cmd[0]](c, int(cmd[1:]))
    return parsed_cmd


def main():
    running_count, counter = 0, 0
    seen = set()  # just for efficient lookup I guess
    with open('day_01/day_1_input.txt') as f:
        all_cmds = f.read().split('\n')

    while True:
        cmd = all_cmds[counter % len(all_cmds)]
        running_count = parser(cmd, running_count)
        if running_count in seen:
            break
        seen.add(running_count)
        counter += 1

    print(f"Final result: {running_count}")


if __name__ == '__main__':
    main()
