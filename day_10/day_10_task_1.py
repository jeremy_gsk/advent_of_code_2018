# Day 10 Task 1
# Strategy:
# Difficulty: Medium. Primarily had trouble with printing to grid (deciding what canvas
#   size to use, essentially...)
import re
import time


class Star:
    def __init__(self, position, velocity):
        self.position = list(position)
        self.velocity = list(velocity)

    def move(self):
        self.position[0] = self.position[0] + self.velocity[0]
        self.position[1] = self.position[1] + self.velocity[1]

    def __repr__(self):
        return rf"{self.__class__.__name__}(position={self.position}, velocity={self.velocity})"


def print_message(constellation, max_canvas_size=100):
    min_x = min(s.position[0] for s in constellation)
    min_y = min(s.position[1] for s in constellation)
    constellation_width = max(s.position[0] for s in constellation) - min_x
    constellation_height = max(s.position[1] for s in constellation) - min_y

    if constellation_width > max_canvas_size or constellation_height > max_canvas_size:
        print("Constellation too large. Skipping print...")
        return False

    grid = [[' ' for _ in range(constellation_width + 1)] for _ in range(constellation_height + 1)]
    for star in constellation:
        grid[star.position[1] - min_y][star.position[0] - min_x] = '#'
    print(*[''.join(*zip(*row)) for row in grid], sep='\n')
    return True


def process_constellation(constellation):
    pattern = re.compile(r"position=<([-\s]*\d+),([-\s]*\d+)> velocity=<([-\s]*\d+),([-\s]*\d+)>")
    result = []

    for line in constellation:
        px, py, vx, vy = pattern.match(line).groups()
        result.append(Star(position=(int(px), int(py)), velocity=(int(vx), int(vy))))
    return result


sample_input = """
position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>
"""


def main():
    with open('day_10/day_10_input.txt') as f:
        # constellation = sample_input.strip().split('\n')
        constellation = f.read().strip().split('\n')

    constellation = process_constellation(constellation)

    while True:
        # keep looping and printing the constellation until user cancels themselves
        if print_message(constellation):
            time.sleep(1)
        for star in constellation:
            star.move()


if __name__ == '__main__':
    main()
