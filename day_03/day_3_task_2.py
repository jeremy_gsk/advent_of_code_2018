# Day 3 Task 2
import numpy as np


def main():
    fabric_id = np.zeros((1_000, 1_000), dtype=int)
    sole_id = set()
    with open('day_03/day_3_input.txt') as f:
        all_cmds = f.read().split('\n')

    for cmd in all_cmds:
        # make use of the fact that all cmds are of
        # the form: `#123 @ 3,2: 5x4` to parse the cmd
        ID_hash, _, left_top, wd_ht = cmd.split(' ')
        ID = int(ID_hash[1:])
        left_top = left_top.split(',')
        left, top = int(left_top[0]), int(left_top[1][:-1])
        wd_ht = wd_ht.split('x')
        wd, ht = int(wd_ht[0]), int(wd_ht[1])

        if fabric_id[left:left + wd, top:top + ht].sum() > 0:
            # overlapped
            unique_elms = set((fabric_id[left:left + wd, top:top + ht]).flatten())
            sole_id -= unique_elms
        else:
            # no overlap
            sole_id.add(ID)

        fabric_id[left:left + wd, top:top + ht] = ID

    print(f"The sole ID that does not have overlap is {sole_id}.")


if __name__ == '__main__':
    main()
