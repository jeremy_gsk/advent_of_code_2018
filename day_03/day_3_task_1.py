# Day 3 Task 1
import numpy as np


def main():
    # initialize an oversized board -->
    # visually inspect for max or check programmatically
    fabric = np.zeros((1000, 1000))
    with open('day_03/day_3_input.txt') as f:
        all_cmds = f.read().split('\n')

    for cmd in all_cmds:
        # make use of the fact that all cmds are of
        # the form: `#123 @ 3,2: 5x4` to parse the cmd
        _, _, left_top, wd_ht = cmd.split(' ')
        left_top = left_top.split(',')
        left, top = int(left_top[0]), int(left_top[1][:-1])
        wd_ht = wd_ht.split('x')
        wd, ht = int(wd_ht[0]), int(wd_ht[1])

        # update fabric cells
        fabric[left:left + wd, top:top + ht] += 1

    print((fabric > 1).sum())


if __name__ == '__main__':
    main()
