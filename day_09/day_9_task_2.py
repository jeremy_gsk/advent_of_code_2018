# Day 9 Task 2
# Strategy: Main focus on efficiency, so rethink the approach. The most time consuming
#   part is insertion. (Gave up, this solution was from Reddit, trick is to use deque's
#   rotate method.)
# Difficulty: Hard
from collections import deque


# TEST CASES
# NUM_PLAYERS, LAST_MARBLE_PTS = 9, 25  # 32
# NUM_PLAYERS, LAST_MARBLE_PTS = 10, 1618  # 8317
# NUM_PLAYERS, LAST_MARBLE_PTS = 13, 7999  # 146373
# NUM_PLAYERS, LAST_MARBLE_PTS = 17, 1104  # 2764
# NUM_PLAYERS, LAST_MARBLE_PTS = 21, 6111  # 54718
# NUM_PLAYERS, LAST_MARBLE_PTS = 30, 5807  # 37305

# NUM_PLAYERS, LAST_MARBLE_PTS = 473, 70904  # ??
NUM_PLAYERS, LAST_MARBLE_PTS = 473, 70904 * 100  # ??


def main():
    player_points = [0 for _ in range(NUM_PLAYERS)]
    circle = deque([0])

    for marble in range(1, LAST_MARBLE_PTS + 1):
        if marble % 23 == 0:
            circle.rotate(7)
            player_points[marble % NUM_PLAYERS] += marble + circle.pop()
            circle.rotate(-1)
        else:
            circle.rotate(-1)
            circle.append(marble)

    print("Winning elf score:", max(player_points))


if __name__ == '__main__':
    main()
