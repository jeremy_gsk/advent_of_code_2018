# Day 9 Task 1
# Strategy:
# Difficulty: Task 1 is deceptively Easy


# TEST CASES
# NUM_PLAYERS, LAST_MARBLE_PTS = 9, 25  # 32
# NUM_PLAYERS, LAST_MARBLE_PTS = 10, 1618  # 8317
# NUM_PLAYERS, LAST_MARBLE_PTS = 13, 7999  # 146373
# NUM_PLAYERS, LAST_MARBLE_PTS = 17, 1104  # 2764
# NUM_PLAYERS, LAST_MARBLE_PTS = 21, 6111  # 54718
# NUM_PLAYERS, LAST_MARBLE_PTS = 30, 5807  # 37305

NUM_PLAYERS, LAST_MARBLE_PTS = 473, 70904  # ??


def main():

    marbles = [0]
    player_points = [0 for _ in range(NUM_PLAYERS)]

    current_marble = 0
    current_idx = 0
    current_player = -1

    while current_marble < LAST_MARBLE_PTS:
        current_marble += 1
        current_player = (current_player + 1) % NUM_PLAYERS

        left = (current_idx + 1) % len(marbles)
        right = (current_idx + 2) % len(marbles)
        if current_marble % 23 == 0:
            idx_to_remove = (current_idx - 7) % len(marbles)
            player_points[current_player] += current_marble + marbles[idx_to_remove]
            marbles = marbles[:idx_to_remove] + marbles[idx_to_remove + 1:]
            current_idx = idx_to_remove
        elif left == len(marbles) - 1:
            # we're at the end of the marbles list, just append new marble to the end
            marbles.append(current_marble)
            current_idx = left + 1
        else:
            marbles.insert(right, current_marble)
            current_idx = right

        # debugging
        # marbles_string = [str(m) if m != current_marble else f"({m})" for m in marbles]
        # print(f"[{current_player + 1}]", " ".join(marbles_string))

    print("Winning elf score:", max(player_points))


if __name__ == '__main__':
    main()
