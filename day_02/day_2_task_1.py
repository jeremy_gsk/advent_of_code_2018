# Day 2 Task 1
from collections import Counter


def main():
    count_twice, count_thrice = 0, 0
    with open('day_02/day_2_input.txt') as f:
        all_cmds = f.read().split('\n')

    for cmd in all_cmds:
        c_dict = Counter(cmd)  # returns a dictionary of letters and their counts
        if 2 in c_dict.values(): count_twice += 1
        if 3 in c_dict.values(): count_thrice += 1

    checksum = count_twice * count_thrice
    print(f"Checksum: {checksum}")


if __name__ == '__main__':
    main()
