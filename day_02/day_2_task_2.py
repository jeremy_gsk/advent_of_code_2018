# Day 2 Task 2
import itertools as it


def main():
    with open('day_02/day_2_input.txt') as f:
        all_cmds = f.read().split('\n')

    for a, b in it.combinations(all_cmds, 2):
        assert len(a) == len(b)
        if sum([a[i] != b[i] for i in range(len(a))]) == 1:
            # cmds a and b differ by only 1 letter
            break

    print(f"The two box IDs are {a} and {b}.")
    print(f"Common letters: "
          f"{''.join([char1 for char1, char2 in zip(a, b) if char1 == char2])}")


if __name__ == '__main__':
    main()
