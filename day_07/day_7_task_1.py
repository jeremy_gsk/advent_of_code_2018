# Day 7 Task 1
# Strategy: Maintain a list of dependencies in a dict (like an Adjacency list), i.e.,
#   {"C": ["A", "B"], "A": ["E"]} means step "C" depends on the completion of "A" and
#   "B". In this formulation, the root(s) of the DAG will have empty dependency lists.
#   We also make note of what steps have been completed thus far; If a node's dependencies
#   have all been completed, then add the current child node to the final steps list.
#   In order to implement the "alphabetical" rule, we just make use of the fact that
#   Python dictionaries are ordered by key insertion order.
import re
import string


sample_input = """
Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.
"""


def parse_into_dependencies(instructions):
    """
    Parameters
    ----------
    instructions (list[str]):
        List of instructions from the input, each instruction is a string of a certain
        format.

    Returns
    -------
    dependencies (dict[str, set[str]]):
        where dictionary key is the child node in the dag, while the dictionary value is a set
        of parents that the child node is dependent on, i.e., child node can only run once parent nodes
        are completed.
    """
    # here, we already assume there can only be the 26 letters of the alphabet referenced inside
    # instructions. the logic below will fail otherwise (needs some more careful consideration)
    # also impt: dictionary (keys) is ordered alphabetically.
    dependencies = {s: set() for s in string.ascii_uppercase}
    pattern = re.compile(r"Step ([A-Z]) must be finished before step ([A-Z]) can begin.")
    seen_steps = set()

    for instruction in instructions:
        parent, child = pattern.match(instruction).groups()
        dependencies[child].add(parent)
        seen_steps.update(child, parent)

    for letter in set(string.ascii_uppercase) - seen_steps:
        dependencies.pop(letter)

    return dependencies


def main():
    with open('day_07/day_7_input.txt') as f:
        # instructions = sample_input.strip().split('\n')
        instructions = f.read().strip().split('\n')

    dependencies = parse_into_dependencies(instructions)
    completed_steps = []

    while dependencies.keys():
        for child, parents in dependencies.items():
            if not (parents - set(completed_steps)):
                # means all dependencies are completed
                completed_steps.append(child)
                dependencies.pop(child)
                break

    completed_steps = ''.join(completed_steps)
    print(f"Steps: {completed_steps}")


if __name__ == '__main__':
    main()
