# Day 7 Task 2
# Strategy:
import string
from day_7_task_1 import parse_into_dependencies


sample_input = """
Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.
"""
NUM_WORKERS = 5
PLUS_TIME = 60


def main():
    with open('day_07/day_7_input.txt') as f:
        # instructions = sample_input.strip().split('\n')
        instructions = f.read().strip().split('\n')

    dependencies = parse_into_dependencies(instructions)
    alphnum_dict = dict(zip(string.ascii_uppercase, range(PLUS_TIME + 1, PLUS_TIME + 27)))
    time_step = -1

    # NOTE: although not necessary for the task, I choose to keep track of the steps being performed
    #   as well as the order of the steps being completed for easier debugging.
    #   Otherwise, `worker_countdown` can just be a list (of time units).
    worker_countdown = {}  # maintain a list of workers and time left for the tasks being performed
    completed_steps = []

    # keep event loop going as long as there are dependencies left or workers are still running
    while dependencies or worker_countdown:
        time_step += 1

        # consume tasks
        for step, t in list(worker_countdown.items()):
            worker_countdown[step] -= 1  # countdown
            # then check if workers' tasks are done, if so, complete the step
            # and empty the worker
            if t - 1 <= 0:
                completed_steps.append(step)
                worker_countdown.pop(step)

        # produce tasks
        for child, parents in list(dependencies.items()):
            if not (parents - set(completed_steps)):
                # means all dependencies are completed, try to find a worker
                # to start processing child
                if len(worker_countdown) < NUM_WORKERS:
                    worker_countdown[child] = alphnum_dict[child]
                    dependencies.pop(child)

        # debug: print out the consumer/producer state at the end of this time step
        # print(time_step, worker_countdown, completed_steps)

    completed_steps = ''.join(completed_steps)
    print(f"Steps completed in {time_step} units of time: {completed_steps}")


if __name__ == '__main__':
    main()
