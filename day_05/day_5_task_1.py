# Day 5 Task 1
# Test case: dabAcCaCBAcCcaDA --> 10
import re
import string


def react(s: str) -> str:
    valid = '|'.join([''.join(t) for t in zip(string.ascii_uppercase, string.ascii_lowercase)] +
                     [''.join(t) for t in zip(string.ascii_lowercase, string.ascii_uppercase)])
    pattern = re.compile(valid)

    # parse polymer
    ############################
    # -- first attempt: slow!
    # matches = pattern.search(s)
    # while matches is not None:
    #     s = s[:matches.start()] + s[matches.end():]
    #     matches = pattern.search(s)

    # -- second attempt:
    while len(pattern.findall(s)) > 0:
        s = pattern.sub(repl='', string=s, count=15000)
    ############################
    return s


def main():
    with open('day_05/day_5_input.txt') as f:
        polymer = f.read()

    polymer = react(polymer)
    print(f"Resulting polymer is of length {len(polymer)}:\n{polymer}")


if __name__ == '__main__':
    main()
