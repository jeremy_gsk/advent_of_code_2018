# Day 5 Task 2
# Test case: dabAcCaCBAcCcaDA -> 4
import re
import string
from day_5_task_1 import react


def main():
    alph_pairs = ['|'.join(t) for t in zip(string.ascii_uppercase, string.ascii_lowercase)]
    with open('day_05/day_5_input.txt') as f:
        polymer = f.read()

    shortest_len = len(polymer)
    for ap in alph_pairs:
        pattern = re.compile(ap)
        poly_tmp = pattern.sub(repl='', string=polymer, count=5000)
        assert len(pattern.findall(poly_tmp)) == 0, \
            f'Raise the sub count by {len(pattern.findall(poly_tmp))}!'
        new_len = len(react(poly_tmp))
        print(f"Removing {ap} and fully reacting results in length {new_len}.")
        if new_len < shortest_len:
            shortest_len = new_len

    print(f"Shortest length is {shortest_len}.")


if __name__ == '__main__':
    main()
