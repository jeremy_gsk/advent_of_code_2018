# Day 6 Task 1
# Strategy: Take the max coordinates and assume a matrix of that size.
#   Then plot in the coordinates and calculate Manhatten distances.
#   Then find the one with least number of assigments that are not on the
#   edges of the grid. (since those will necessarily extend forever).
from collections import Counter


sample_input = """
1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
"""


def find_closest_point_id(points_dict, coordinates) -> int:
    """For given `coordinates` (x, y), return the id of the closest point in `points_dict`
    by Manhatten distance, or -1 if is equidistant to 2 or more points.

    Parameters
    ----------
    points_dict (dict[int, tuple[int,int]):
        Dictionary containing the points' id (int) as the dict key, the points' 2D coordinates
        as the value.

    coordinates (tuple[int, int]):
        (x, y) coordinates to calculate Manhatten distance for.
    """
    x, y = coordinates
    distances = {}
    for pid, (px, py) in points_dict.items():
        dist = abs(px - x) + abs(py - y)
        distances[pid] = dist

    manhatten_dist = min(distances.values())
    manhatten_pids = [pid for pid, dist in distances.items() if dist == manhatten_dist]
    return -1 if len(manhatten_pids) > 1 else manhatten_pids[0]


def main():
    with open('day_06/day_6_input.txt') as f:
        # raw_points = sample_input.strip().split('\n')
        raw_points = f.read().strip().split('\n')

    # generate dictionary of points {pid: (x, y)} from input txt data
    # pid is just some integer id we assign to identify the given coordinate later
    points = {}
    min_x, min_y, max_x, max_y = 999, 999, -1, -1
    for idx, pt in enumerate(raw_points, start=1):
        x, y = tuple(map(int, pt.split(', ')))
        points[idx] = (x, y)
        min_x = min(x, min_x)
        min_y = min(y, min_y)
        max_x = max(x, max_x)
        max_y = max(y, max_y)

    # calculate Manhatten distance for every point in the grid
    pids_on_the_edges = set()
    pid_votes = []
    for ix in range(min_x, max_x + 1):
        for iy in range(min_y, max_y + 1):
            manhatten_pid = find_closest_point_id(points, coordinates=(ix, iy))

            # no singular closest point
            if manhatten_pid == -1:
                continue
            # note down the pid if we're currently calculating for the edges of the grid
            # we want to ignore these later
            if (ix == min_x) or (ix == max_x) or (iy == min_y) or (iy == max_y):
                pids_on_the_edges.add(manhatten_pid)
            pid_votes.append(manhatten_pid)

    # if a vote is coming from a point on the edge of the grid, it must mean that this
    # voted pid will have infinite area.
    pid_votes = Counter([pid for pid in pid_votes if pid not in pids_on_the_edges])
    largest_area = pid_votes.most_common(1)[0][1]

    print(f"Largest non-infinite area is {largest_area}")


if __name__ == '__main__':
    main()
