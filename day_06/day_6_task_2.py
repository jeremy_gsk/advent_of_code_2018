# Day 6 Task 2
# Strategy: The main challenge in this task is determining how far / how large a grid to
#   search. My hypothesis is that the grid size to search should be around (or at least,
#   an upper bound) THRESHOLD_DIST / len(coordinates) on the left AND right of the given
#   points. The main intuition being that the sum of Manhatten distances should be less
#   than THRESHOLD_DIST, meaning on average, the points we're calculating for shouldn't be
#   located further than the average required Manhatten distance away...

sample_input = """
1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
"""


def calculate_manhatten_distance_sum(points_dict, coordinates) -> int:
    """For given `coordinates` (x, y), return the sum of Manhatten distances to all of the
    points in `points_dict`.

    Parameters
    ----------
    points_dict (dict[int, tuple[int,int]):
        Dictionary containing the points' id (int) as the dict key, the points' 2D coordinates
        as the value.

    coordinates (tuple[int, int]):
        (x, y) coordinates to calculate Manhatten distance for.
    """
    x, y = coordinates
    total_dist = 0
    for (px, py) in points_dict.values():
        total_dist += abs(px - x) + abs(py - y)

    return total_dist


def main():
    with open('day_06/day_6_input.txt') as f:
        # raw_points = sample_input.strip().split('\n')
        raw_points = f.read().strip().split('\n')

    THRESHOLD_DIST = 10_000
    buffer = 200  # NOTE: read the strategy above

    # generate dictionary of points {pid: (x, y)} from input txt data
    # pid is just some integer id we assign to identify the given coordinate later
    points = {}
    min_x, min_y, max_x, max_y = 999, 999, -1, -1
    for idx, pt in enumerate(raw_points, start=1):
        x, y = tuple(map(int, pt.split(', ')))
        points[idx] = (x, y)
        min_x = min(x, min_x)
        min_y = min(y, min_y)
        max_x = max(x, max_x)
        max_y = max(y, max_y)

    # calculate Manhatten distance for every point in the grid
    region_area = 0
    for ix in range(min_x - buffer, max_x + buffer + 1):
        for iy in range(min_y - buffer, max_y + buffer + 1):
            dist_sum = calculate_manhatten_distance_sum(points, coordinates=(ix, iy))
            if dist_sum < THRESHOLD_DIST:
                region_area += 1

    print(f"Region area with <{THRESHOLD_DIST} Manhatten distance is {region_area}")


if __name__ == '__main__':
    main()
