# Day 8 Task 2
# Strategy:
from collections import namedtuple
from day_8_task_1 import process_nodes


sample_input = """
2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
"""

Node = namedtuple('Node', 'header children metadata')


def get_value_of_node(node):
    if not node.children:
        return sum(node.metadata)

    total_value = 0
    for idx in node.metadata:
        try:
            total_value += get_value_of_node(node.children[idx - 1])
        except IndexError:
            # child node does not exist; just continue
            pass

    return total_value


def main():
    with open('day_08/day_8_input.txt') as f:
        # numbers = list(map(int, sample_input.strip().split()))
        numbers = list(map(int, f.read().strip().split()))

    _, nodes, _ = process_nodes(numbers, nodes=[])

    # debugging
    # print("List of nodes:\n", nodes)

    # find root node and its value
    # NOTE: this is fragile logic; because of the way I wrote my function, the
    # root node will always be the last node to be added into the nodes list.
    root_node = nodes[-1]
    value = get_value_of_node(root_node)

    print(f"Value of root node is {value}")


if __name__ == '__main__':
    main()
