# Day 8 Task 1
# Strategy: I found this pretty challenging.. Main thought process was to process it recursively;
#   took a lot of trial&error and refactoring to get right. Note that while keeping track of
#   each nodes' children is not compulsory in task 1, we need it for task 2.
from collections import namedtuple


sample_input = """
2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
"""

Node = namedtuple('Node', 'header children metadata')


def process_nodes(numbers, nodes):
    num_child, num_metadata, *numbers = numbers
    to_process = num_child
    all_children = []

    while to_process > 0:
        numbers, nodes, child = process_nodes(numbers, nodes)
        all_children.append(child)
        to_process -= 1

    metadata, numbers = numbers[:num_metadata], numbers[num_metadata:]
    node_to_add = Node(header=(num_child, num_metadata), children=all_children, metadata=metadata)
    nodes.append(node_to_add)

    return numbers, nodes, node_to_add


def main():
    with open('day_08/day_8_input.txt') as f:
        numbers = list(map(int, sample_input.strip().split()))
        # numbers = list(map(int, f.read().strip().split()))

    _, nodes, _ = process_nodes(numbers, nodes=[])

    # debugging
    # print("List of nodes:\n", nodes)

    # sum metadata
    total_sum = 0
    for n in nodes:
        total_sum += sum(n.metadata)

    print(f"Metadata sum is {total_sum}")


if __name__ == '__main__':
    main()
